Hay que analizar que dimension de la base de datos queremos analizar.

Aspectos posibles a analizar;

- ¿Cantidad de recaudacion por territorio?
- ¿Que shipper realiza mas envios?
- ¿Que shipper realiza mas envios en un territorio especifico?
    - ES.Region_Dim  
    - ES.Shippers_Dim
    - ES.Shippings_Fact
    - ES.Territories_Dim
    - ES.Time_Dim

- Ganancias por cliente, categoria, producto y tiempo.
    - SC.Categoy_Dim
    - SC.Customer_Dim
    - SC.Products_Dim
    - SC.Sales_Fact

Realizar una base de datos warehouse orientada a responder las preguntas que planteamos

HAY QUE HACER MODIFICACIONES EN EL MODELO, TENER EN CUENTA:
- En las Fact tiene que haber campos calculables solamente, y los ID a las Dim.
- Las Dim son parecidas a las tablas relacionales, solo que con campos mas desnormalizados y solo los que necesito

Nota: Los campos calculados van en el cubo, no en la Fact

- hacer el etl
- el etl es un programa que migra los datos de la base hacia el warehouse
- luego hay que realizar el cubo
- el cubo es lo que nutre a nuestros reportes

- respetar tipo de datos cuando pasamos los datos al warehouse

Documento ejemplo para hacer un DW con Northwind:

https://medium.com/@kmsbmadhan/dimensional-modelling-visualization-of-northwind-database-beaac7fecb20

ssis
https://www.youtube.com/watch?v=vSXplK9h8u8

Como crear un cubo olap;
https://www.youtube.com/watch?v=ctUiHZHr-5M